= Chapitre 04 : Active Directory
:title-page:
:author: Arnaud Harbonnier
:email: arnaud.harbonnier@ltpdampierre.fr 
//firstname middlename lastname <email>firstname middlename lastname <email>; firstname middlename lastname <email>
//Arnaud Harbonnier <arnaud.harbonnier@gmail.com>
//Arnaud Harbonnier <arnaud.harbonnier@valarep.fr>
//Arnaud Harbonnier <arnaud.harbonnier@ltpdampierre.fr>
//doctype: book
:sectnums:
:toc:
:toc-title: SOMMAIRE
:toclevels: 2
:source-highlighter: highlightjs
:icons: font
//:icon-set: fab
:imagesdir: ./images/B3-04
:sectanchors: 
//image::sunset.jpg[Sunset,300,400]
//image::sunset.jpg[alt=Sunset,width=300,height=400]
:revnumber: 12-2023
:revdate: Lycée Dampierre
:revremark: BTS SIO 1A
//:version-label!: 
//:version-label: Edition
:experimental:
//diagram-type::source-file-name[format=output-format]
////
:icons: font
:icon-set: fas
You can use font-based icons from any of the following icon sets in your PDF document:
    fa - Font Awesome 4 (default, deprecated)
    fas - Font Awesome - Solid
    fab - Font Awesome - Brands
    far - Font Awesome - Regular
    fi - Foundation Icons
    pf - Payment font (deprecated)
////
// saut de page <<<


== Installation du rôle ADDS

=== Etape 1 : Installation du rôle

image:ROOT:B3-04/2023-11-29T13-24-24-369Z.png[] 

image:ROOT:B3-04/2023-11-29T13-25-10-039Z.png[] 

image:ROOT:B3-04/2023-11-29T13-25-37-553Z.png[] 

=== Etape 2 : Configuration du rôle

image:ROOT:B3-04/2023-11-29T13-27-03-677Z.png[] 

image:ROOT:B3-04/2023-11-29T13-29-57-123Z.png[] 

image:ROOT:B3-04/2023-11-29T13-30-47-357Z.png[] 

image:ROOT:B3-04/2023-11-29T13-31-18-967Z.png[] 

image:ROOT:B3-04/2023-11-29T13-31-54-382Z.png[] 

image:ROOT:B3-04/2023-11-29T13-32-14-635Z.png[] 

image:ROOT:B3-04/2023-11-29T13-32-38-199Z.png[] 

[listing]
#
# Script Windows PowerShell pour le déploiement d’AD DS
#
Import-Module ADDSDeployment
Install-ADDSForest `
-CreateDnsDelegation:$false `
-DatabasePath "C:\Windows\NTDS" `
-DomainMode "WinThreshold" `
-DomainName "dampierre.local" `
-DomainNetbiosName "DAMPIERRE" `
-ForestMode "WinThreshold" `
-InstallDns:$true `
-LogPath "C:\Windows\NTDS" `
-NoRebootOnCompletion:$false `
-SysvolPath "C:\Windows\SYSVOL" `
-Force:$true

image:ROOT:B3-04/2023-11-29T13-34-02-327Z.png[] 

== La console _Utilisateurs et ordinateurs Active Directory_

[NOTE]
Après redémarrage, les comptes locaux ont disparu

image:ROOT:B3-04/2023-11-29T13-42-45-877Z.png[] 

UPN

image:ROOT:B3-04/2023-11-29T13-43-28-796Z.png[] 

image:ROOT:B3-04/2023-11-29T13-44-23-392Z.png[] 

image:ROOT:B3-04/2023-11-29T13-45-19-796Z.png[] 

image:ROOT:B3-04/2023-11-29T13-46-02-036Z.png[] 

image:ROOT:B3-04/2023-11-29T13-49-28-281Z.png[] 

== Conséquences ADDS

=== Suppression des comptes locaux sur SRV01

=== Ajout du 127.0.0.1 sur le DC

AD fonctionne avec DNS donc il doit localiser le DNS local afin de situer le DC.
SRV01 héberge ici, ce n'est pas obligatoire, les services AD *ET* le DNS

image:ROOT:B3-04/2023-11-29T13-53-29-169Z.png[] 

=== Arrêt du DHCP

Le client récupère une APIPA..

Contrôle du DHCP

image:ROOT:B3-04/2023-12-06T08-57-35-910Z.png[] 

L'installation d'AD a arrêter le service DHCP, ce dernier doit être autoriser.

image:ROOT:B3-04/2023-11-29T13-56-29-254Z.png[] 

image:ROOT:B3-04/2023-12-06T08-59-22-541Z.png[] 

modifier l'adresse du dns pour intégrer le client au domaine.

image:ROOT:B3-04/2023-12-06T09-05-22-604Z.png[] 

== Gestion des objets Active Directory

=== Les ordinateurs

Migration d'un client dans le domaine

image:ROOT:B3-04/2023-12-06T08-19-01-088Z.png[] 

image:ROOT:B3-04/2023-12-06T08-56-25-877Z.png[] 

[WARNING]
Le client doit recevoir le DNS local afin qu'il puisse localiser le DC (services d'annuaire).

=== Les unités d'organisation

Elles permettent d'avoir une granularité au niveau de notre domaine et apporter aussi de la lisibilité dans la console.

=== Les comptes utilisateurs

=== les groupes

Voir les notions de type (sécurité & distribution) et d'étendue (domaine local & globale)

==== Les groupes de domaine local

==== Les groupes globaux

